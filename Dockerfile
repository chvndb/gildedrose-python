FROM python:3.8.0-slim-buster
MAINTAINER Christophe Vandenberghe <cva@brick57.be>

COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt

WORKDIR /app
COPY ./pytest.ini ./
COPY ./entrypoint.sh ./
COPY ./gilded_rose ./gilded_rose
COPY ./tests ./tests

ENTRYPOINT ["/app/entrypoint.sh"]
