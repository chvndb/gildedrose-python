all: pytest

init:
	pip install -r requirements.txt

pylint:
	pylint gilded_rose tests

flake8:
	flake8 --show-source .

pytest:
	pytest --cov=gilded_rose tests/

dockerized:
	docker build -q -t gilded_rose_python .
	docker run -ti --rm gilded_rose_python
