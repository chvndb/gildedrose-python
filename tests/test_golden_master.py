from __future__ import print_function

import os

from gilded_rose import GildedRose, Item


def run_gilded_rose(days):
    items = [
        Item(name="+5 Dexterity Vest", sell_in=10, quality=20),
        Item(name="Aged Brie", sell_in=2, quality=0),
        Item(name="Elixir of the Mongoose", sell_in=5, quality=7),
        Item(name="Sulfuras, Hand of Ragnaros", sell_in=0, quality=80),
        Item(name="Sulfuras, Hand of Ragnaros", sell_in=-1, quality=80),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=10, quality=49),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49),
        Item(name="Conjured Mana Cake", sell_in=3, quality=6),  # <-- :O
    ]

    for day in range(days):
        print("-------- day %s --------" % day)
        print("name, sellIn, quality")
        for item in items:
            print(item)
        print("")
        GildedRose(items).update_quality()


def test_golden_master_30_days(capfd):
    run_gilded_rose(30)
    output = capfd.readouterr()[0]
    filename = os.path.join('tests', 'golden_master_30_days.txt')
    if not os.path.isfile(filename):
        f = open(filename, "w")
        f.write(output)
        f.close()
    golden = open(filename, "r").read()
    assert output == golden
