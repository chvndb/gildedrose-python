'''
The gilded rose implementation that couples items to update strategies
'''


from gilded_rose.items import AGED_BRIE, BACKSTAGE_PASSES, SULFURAS, CONJURED
from gilded_rose.strategies import DefaultStrategy, AgedBrieStrategy, BackstagePassesStrategy, SulfurasStrategy, ConjuredStrategy


STRATEGIES = {
    AGED_BRIE: AgedBrieStrategy(),
    BACKSTAGE_PASSES: BackstagePassesStrategy(),
    SULFURAS: SulfurasStrategy(),
    CONJURED: ConjuredStrategy()
}


class GildedRose:

    def __init__(self, items):
        self.items = items

    def find_strategy(self, item):
        for special_item, strategy in STRATEGIES.items():
            if special_item.matches(item):
                return strategy
        return DefaultStrategy()

    def update_quality(self):
        for item in self.items:
            strategy = self.find_strategy(item)
            strategy.update_quality(item)
            strategy.update_sell_in(item)
