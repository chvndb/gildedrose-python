'''
Defines items and special items that require specific update startegies
'''

from enum import Enum


class Item:
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)


class SpecialItems(Enum):
    AGED_BRIE = "Aged Brie"
    BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert"
    SULFURAS = "Sulfuras, Hand of Ragnaros"
    CONJURED = "Conjured"

    def matches(self, item):
        return item.name.startswith(self.value)

    def __str__(self):
        return self.value


globals().update(SpecialItems.__members__)
