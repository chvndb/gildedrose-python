#!/bin/sh

if [ $# -eq 0 ]
  then pytest --cov=gilded_rose tests/
  else $@
fi
